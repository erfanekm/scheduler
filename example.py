
import time

class scheduler():
    f_name = []
    f_args = []
    f_period = []
    f_last = []
    to_run = dict()
    resolution = 1.0 # seconds

    def __init__(self):
        pass

    def floor_time(self, tme):
        floortme = int(tme / self.resolution) * self.resolution
        return floortme


    def add(self, name, args, period, start):
        self.f_name.append(name)
        self.f_args.append(args)
        prd = self.floor_time(period)
        self.f_period.append(prd)
        tme = self.floor_time(start)
        self.f_last.append(tme)
        f_number = len(self.f_name)

        self.to_run[tme] = f_number

    def step(self):
        tme = time.time()
        floortme = self.floor_time(tme)
        